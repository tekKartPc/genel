import RPi.GPIO as GPIO
import time

hangiPin = 18

GPIO.setmode(GPIO.BOARD)
GPIO.setup(hangiPin,GPIO.OUT)

try:

	while True:

		GPIO.output(hangiPin,True)
 		time.sleep(1)
 		GPIO.output(hangiPin,False)
 		time.sleep(1)

except KeyboardInterrupt:

	GPIO.output(hangiPin,False)
	print(" Buzz deneme den cikis yapildi")

finally:
	GPIO.cleanup()
